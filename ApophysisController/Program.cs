﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
// ReSharper disable ConditionIsAlwaysTrueOrFalse


namespace ApophysisController
{
    internal class Program
    {
        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr point);

        private static void Main()
        {
            const bool shutdownOnComplete = true;
            const string renderPath = "C:\\AutoApo\\";
            const int width = 6000;
            const int quality = 4000;
            const int oversample = 5;


            Console.WriteLine("Starting Apophysis...");
            using (var proc = Process.Start(new ProcessStartInfo
            {
                Arguments = "",
                FileName = "Apophysis-2.09.exe",
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true
            }))
            {
                //alt is percent sign
                //ctrl is carrot
                //Tab is \t
                System.IO.Directory.CreateDirectory(renderPath);
                Console.WriteLine("Waiting a while...");
                Thread.Sleep(5000);
                Console.WriteLine("Sending Keys...");
                SendKeys.SendWait("%{o}");
                SendKeys.SendWait("{o}");
                SendKeys.SendWait("^+\t");
                SendKeys.SendWait("^+\t");
                SendKeys.SendWait(Environment.ProcessorCount.ToString());
                SendKeys.SendWait("{ENTER}");
                SendKeys.SendWait("%{l}");
                SendKeys.SendWait("{d}");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                if (shutdownOnComplete)
                    SendKeys.SendWait(" ");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait(renderPath);
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait(width.ToString());
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait(quality.ToString());
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                for (var i = 0; i < 100; i++)
                    SendKeys.SendWait("{DOWN}");
                for (var i = 1; i < oversample; i++)
                    SendKeys.SendWait("{UP}");
                SendKeys.SendWait("\t");
                SendKeys.SendWait(" ");
                SendKeys.SendWait("\t");
                for (var i = 0; i < 10; i++)
                    SendKeys.SendWait("{UP}");
                for (var i = 0; i < 4; i++)
                    SendKeys.SendWait("{DOWN}");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait("\t");
                SendKeys.SendWait(" ");

                Console.WriteLine("Waiting a while...");
                Thread.Sleep(4000);
                //proc.Kill();
                proc.WaitForExit();

                // Retrieve the app's exit code
                var exitCode = proc.ExitCode;
            }
        }
        
    }
}
