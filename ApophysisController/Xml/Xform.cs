﻿using System.Xml.Serialization;

namespace ApophysisController.Xml
{
    [XmlRoot(ElementName = "xform")]
    public class Xform
    {
        [XmlAttribute(AttributeName = "weight")]
        public string Weight { get; set; }
        [XmlAttribute(AttributeName = "color")]
        public string Color { get; set; }
        [XmlAttribute(AttributeName = "waves")]
        public string Waves { get; set; }
        [XmlAttribute(AttributeName = "fisheye")]
        public string Fisheye { get; set; }
        [XmlAttribute(AttributeName = "coefs")]
        public string Coefs { get; set; }
        [XmlAttribute(AttributeName = "linear")]
        public string Linear { get; set; }
        [XmlAttribute(AttributeName = "sinusoidal")]
        public string Sinusoidal { get; set; }
        [XmlAttribute(AttributeName = "spherical")]
        public string Spherical { get; set; }
        [XmlAttribute(AttributeName = "horseshoe")]
        public string Horseshoe { get; set; }
        [XmlAttribute(AttributeName = "swirl")]
        public string Swirl { get; set; }
        [XmlAttribute(AttributeName = "polar")]
        public string Polar { get; set; }
        [XmlAttribute(AttributeName = "disc")]
        public string Disc { get; set; }
        [XmlAttribute(AttributeName = "diamond")]
        public string Diamond { get; set; }
        [XmlAttribute(AttributeName = "spiral")]
        public string Spiral { get; set; }
        [XmlAttribute(AttributeName = "popcorn")]
        public string Popcorn { get; set; }
        [XmlAttribute(AttributeName = "bent")]
        public string Bent { get; set; }
        [XmlAttribute(AttributeName = "julia")]
        public string Julia { get; set; }
        [XmlAttribute(AttributeName = "handkerchief")]
        public string Handkerchief { get; set; }
        [XmlAttribute(AttributeName = "heart")]
        public string Heart { get; set; }
        [XmlAttribute(AttributeName = "hyperbolic")]
        public string Hyperbolic { get; set; }
    }
}