﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ApophysisController.Xml
{
    [XmlRoot(ElementName = "Flames")]
    public class Flames
    {
        [XmlElement(ElementName = "flame")]
        public List<Flame> Flame { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}