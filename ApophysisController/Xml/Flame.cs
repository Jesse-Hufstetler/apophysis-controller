﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ApophysisController.Xml
{
    [XmlRoot(ElementName = "flame")]
    public class Flame
    {
        [XmlElement(ElementName = "xform")]
        public List<Xform> Xform { get; set; }
        [XmlElement(ElementName = "palette")]
        public Palette Palette { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "size")]
        public string Size { get; set; }
        [XmlAttribute(AttributeName = "center")]
        public string Center { get; set; }
        [XmlAttribute(AttributeName = "scale")]
        public string Scale { get; set; }
        [XmlAttribute(AttributeName = "zoom")]
        public string Zoom { get; set; }
        [XmlAttribute(AttributeName = "oversample")]
        public string Oversample { get; set; }
        [XmlAttribute(AttributeName = "filter")]
        public string Filter { get; set; }
        [XmlAttribute(AttributeName = "quality")]
        public string Quality { get; set; }
        [XmlAttribute(AttributeName = "background")]
        public string Background { get; set; }
        [XmlAttribute(AttributeName = "brightness")]
        public string Brightness { get; set; }
        [XmlAttribute(AttributeName = "gamma")]
        public string Gamma { get; set; }
        [XmlAttribute(AttributeName = "gamma_threshold")]
        public string GammaThreshold { get; set; }
    }
}
