﻿using System.Xml.Serialization;

namespace ApophysisController.Xml
{
    [XmlRoot(ElementName = "palette")]
    public class Palette
    {
        [XmlAttribute(AttributeName = "count")]
        public string Count { get; set; }
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
        [XmlText]
        public string Text { get; set; }
    }
}